var canvas = document.getElementById("myCanvas");
var context = canvas.getContext("2d");

//height and width of draw area
var cw = canvas.width; //pixels
var ch = canvas.height; //pixels


//Tron moves a charactor on a grid. Make a grid
var gridWidth = 30;
var gridHeight = 30;

var gameGrid = null;
var playerX = 0;
var playerY = 0;

//right = 0
//up = 1
//left = 2
//down = 3
var playerDir = 0;
var playerAlive = true;
var playerScore = 0;

var fpsCount = 0;

function init() {
    gameGrid = CreateGameGrid(gridHeight,gridWidth);

    //player starts in the middle
    playerX = Math.floor(gridWidth/2);
    playerY = Math.floor(gridHeight/2);

    //tell the webpage to listen to keyboard events
    document.addEventListener('keydown',keyDown,false);
}

//create a sizeX x sizeY grid
function CreateGameGrid(sizeY, sizeX) {
    let grid = new Array(sizeY);
    for(i = 0; i < sizeY; i++) {
        grid[i] = new Array(sizeX)

        for(j = 0; j < sizeX; j++) {
            grid[i][j] = false;
        }
    }
    return grid;
}

function gameLoop() {
    update();
    draw();
}

function keyDown(e) {
    //called every time the user presses a key

    if(e.keyCode == 39) { //right arrow
        playerDir = 0;
    } else if(e.keyCode == 38) { //up arrow
        playerDir = 1;
    } else if(e.keyCode == 37) { //left arrow
        playerDir = 2;
    } else if(e.keyCode == 40) { //down arrow
        playerDir = 3;
    }
}

function update() {
    if(playerAlive == false)
    {
        //nothing to process if player is dead
        return;
    }

    if(fpsCount % 4 == 0) {
        //time to update player position

        if(playerDir == 0 ) { //right
            playerX = playerX + 1;
        } else if(playerDir == 1) {
            playerY = playerY - 1;
        } else if(playerDir == 2) {
            playerX = playerX - 1;
        } else if(playerDir == 3) {
            playerY = playerY + 1;
        }

        //test if player is still alive
        if(playerY < 0 || playerY >= gridHeight || playerX < 0 || playerX >= gridWidth)
        {
            //left boundary
            playerAlive = false;
            return;
        }
        
        //check if player has ran into themself
        if(gameGrid[playerY][playerX] == true) {
            playerAlive = false;
            return;
        }

        //update grid
        gameGrid[playerY][playerX] = true;
        playerScore = playerScore + 1;
    }

    fpsCount = fpsCount + 1;
}

function draw() {
    //clear the screen
    context.clearRect(0, 0, canvas.width, canvas.height);
    
    //draw the grid
    var pixelsPerPosX = canvas.width / gridWidth;
    var pixelsPerPosY = canvas.height / gridHeight; 
    for(y = 0; y < gridHeight; y++) {
        for(x = 0; x < gridWidth; x++) {
            if(gameGrid[y][x] == true) {
                pixelsX = x * pixelsPerPosX;
                pixelsY = y * pixelsPerPosY;

                context.fillStyle = "#0f0"; //green
                context.fillRect(pixelsX, pixelsY, pixelsPerPosX, pixelsPerPosY);
            }
        }
    }

    if( playerAlive == false) {
        gameOverMessage = "Game Over. Score: " + playerScore;
        context.fillStyle = "#000"; //black
        context.textAlign = "center";
        context.font = "15px Arial"
        context.fillText(gameOverMessage, cw/2, ch/2);
    }
    else {
        context.fillStyle = "#000"; //black
        context.textAlign = "end";
        context.font = "12px Arial"
        context.fillText(playerScore, cw, pixelsPerPosY);
    }
}

if (typeof (canvas.getContext) !== undefined) {
    init();
    fps = 60;
    setInterval(gameLoop, 1000 / fps);
}